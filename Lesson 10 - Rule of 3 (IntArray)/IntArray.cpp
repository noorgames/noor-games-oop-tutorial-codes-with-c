#include "IntArray.h"
#include <iostream>


void IntArray::copyElements(const int* const from, int* const to, const int count)
{
    for (int i = 0; i < count; ++i)
    {
        to[i] = from[i];
    }
}

bool IntArray::isReallocationNecessary() const
{
    return m_size == m_capacity;
}

void IntArray::reallocateMemory()
{
    m_capacity *= 2;
    int* const tmp = new int[m_capacity];

    copyElements(m_data, tmp, m_size);

    delete[] m_data;
    m_data = tmp;
}

void IntArray::appendInMemory(const int element)
{
    m_data[m_size] = element;
    ++m_size;
}

IntArray::IntArray()
    : m_capacity(10)
    , m_size(0)
    , m_data(new int[m_capacity])
{
}


IntArray::IntArray(const IntArray& other)
    : m_capacity(other.m_capacity)
    , m_size(other.m_size)
    , m_data(new int[m_capacity])
{
    copyElements(other.m_data, m_data, m_size);
}

IntArray& IntArray::operator=(const IntArray& other)
{
    int* const tmp = new int[other.m_capacity];
    copyElements(other.m_data, tmp, other.m_size);

    delete[] m_data;

    m_capacity = other.m_capacity;
    m_size = other.m_size;
    m_data = tmp;

    return *this;
}

void IntArray::appendElement(const int element)
{
    if (isReallocationNecessary())
    {
        reallocateMemory();
    }

    appendInMemory(element);
}

int IntArray::getElement(const int index) const
{
    return m_data[index];
}

int IntArray::getSize() const
{
    return m_size;
}

IntArray::~IntArray()
{
    std::cout << "DTOR" << std::endl;
    delete[] m_data;
}
