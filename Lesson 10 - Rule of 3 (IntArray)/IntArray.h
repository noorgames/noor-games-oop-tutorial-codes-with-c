#ifndef IntArray_h__
#define IntArray_h__

class IntArray
{
private:
    int m_capacity;
    int m_size;
    int* m_data;

    static void copyElements(const int* const from, int* const to, const int count);

    bool isReallocationNecessary() const;
    void reallocateMemory();
    void appendInMemory(const int element);
public:
    IntArray();
    IntArray(const IntArray& other);
    IntArray& operator=(const IntArray& other);
    void appendElement(const int element);
    int getElement(const int index) const;
    int getSize() const;
    ~IntArray();
};


#endif // IntArray_h__

