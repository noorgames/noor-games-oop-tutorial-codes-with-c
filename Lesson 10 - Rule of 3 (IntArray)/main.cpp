#include <iostream>
#include "IntArray.h"

int main()
{

    IntArray assignObject;

    {
        IntArray defaultObject;

        for (int i = 0; i < 100; ++i)
        {
            defaultObject.appendElement(i);
        }

        assignObject = defaultObject;
    }

    IntArray copyObject = assignObject; // <=> IntArray copyObject(assignObject);

    for (int i = 0; i < copyObject.getSize(); ++i)
    {
        std::cout << copyObject.getElement(i) << std::endl;
    }

    return 0;
}