#include <iostream>

class Field1
{
public:
    Field1()
    {
        std::cout << "Field1 CTOR" << std::endl;
    }

    ~Field1()
    {
        std::cout << "Field1 DTOR" << std::endl;
    }
};

class Field2
{
public:
    Field2()
    {
        std::cout << "Field2 CTOR" << std::endl;
    }

    ~Field2()
    {
        std::cout << "Field2 DTOR" << std::endl;
    }
};

class Base
{
private:


public:
    Base()
    {
        std::cout << "Base CTOR" << std::endl;
    }

    ~Base()
    {
        std::cout << "Base DTOR" << std::endl;
    }
};

class Derived : public Base
{
private:
    Field2 m_field2;
    Field1 m_field1;

public:
    Derived()
    {
        std::cout << "Derived CTOR" << std::endl;
    }

    ~Derived()
    {
        std::cout << "Derived DTOR" << std::endl;
    }
};

int main()
{
    Derived b;
    return 0;
}
