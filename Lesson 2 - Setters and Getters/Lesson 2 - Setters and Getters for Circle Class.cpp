#include iostream

class Circle
{
private
	float		m_x;
	float		m_y;
	float		m_r;

public
	float getX() const
	{
		return m_x;
	}

	void setX(float x)
	{
		m_x = x;
	}

	float getY() const
	{
		return m_y;
	}

	void setY(float y)
	{
		m_y = y;
	}

	float getR() const
	{
		return m_r;
	}

	void setR(float r)
	{
		if (r  0)
		{
			m_r = r;
		}
		else
		{
			stdcout  Error Radius should be positive.  stdendl;
		}
	}

	void print()
	{
		stdcout  Circle x =   m_x  , y =   m_y  , r =   m_r  stdendl;
	}
};

int main()
{
	Circle c1;
	c1.setX(0);
	c1.setY(0);
	c1.setR(-10);
	c1.print();

	return 0;
}