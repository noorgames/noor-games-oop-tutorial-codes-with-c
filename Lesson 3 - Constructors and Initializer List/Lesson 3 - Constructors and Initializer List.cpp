#include <iostream>

class Circle
{
private:
	float		m_x;
	float		m_y;
	float		m_r;

public:
	Circle(float x, float y, float r)
		: m_x(x)
		, m_y(y)
		, m_r(r)
	{

	}

	Circle()
		: m_x(0)
		, m_y(0)
		, m_r(1)
	{

	}

	Circle(const Circle& other)
		: m_x(other.m_x)
		, m_y(other.m_y)
		, m_r(other.m_r)
	{

	}

	float getX() const
	{
		return m_x;
	}

	void setX(float x)
	{
		m_x = x;
	}

	float getY() const
	{
		return m_y;
	}

	void setY(float y)
	{
		m_y = y;
	}

	float getR() const
	{
		return m_r;
	}

	void setR(float r)
	{
		if (r > 0)
		{
			m_r = r;
		}
		else
		{
			std::cout << "Error: Radius should be positive." << std::endl;
		}
	}

	void print()
	{
		std::cout << "Circle: x = " << m_x << ", y = " << m_y << ", r = " << m_r << std::endl;
	}
};

int main()
{
	Circle c1(1, 2, 10);
	c1.print();

	Circle c2;
	c2.print();

	Circle c3(c1);
	c3.print();

	return 0;
}