#include <iostream>

class Position
{
private:
	float	m_x;
	float	m_y;

public:
	Position(float x, float y)
		: m_x(x)
		, m_y(y)
	{

	}

	bool isInRange(const Position& other, const float range) const
	{
		const float deltaX = other.m_x - m_x;
		const float deltaY = other.m_y - m_y;
		const float distanceSquare = deltaX * deltaX + deltaY * deltaY;
		const bool isInRange = distanceSquare <= range * range;

		return isInRange;
	}
};

class Hero
{
private:
	Position		m_position;

public:
	Hero()
		: m_position(0, 0)
	{

	}

	Position getPosition() const
	{
		return m_position;
	}
};

class Enemy
{
private:
	Position		m_position;
	float			m_attackRange;

public:
	Enemy()
		: m_position(10, 0)
		, m_attackRange(5)
	{

	}

	void attackHero(const Hero& hero) const
	{
		std::cout << "Enemy attacks..." << std::endl;

		const Position heroPosition = hero.getPosition();
		const bool isInRange = m_position.isInRange(heroPosition, m_attackRange);

		if (isInRange)
		{
			std::cout << "Game Over, Hero is dead." << std::endl;
		}
		else
		{
			std::cout << "Hero is still alive." << std::endl;
		}
	}
};

int main()
{
	Hero spiderman;
	Enemy greenGoblin;

	std::cout << "Game is starting..." << std::endl;

	greenGoblin.attackHero(spiderman);

	return 0;
}