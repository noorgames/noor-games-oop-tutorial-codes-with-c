#ifndef Enemy_h
#define Enemy_h

#include <iostream>
#include "Position.cpp"
#include "Hero.cpp"

class Enemy
{
private:
	Position		m_position;
	float			m_attackRange;

public:
	Enemy()
		: m_position(10, 0)
		, m_attackRange(5)
	{

	}

	void attackHero(const Hero& hero) const
	{
		std::cout << "Enemy attacks..." << std::endl;

		const Position heroPosition = hero.getPosition();
		const bool isInRange = m_position.isInRange(heroPosition, m_attackRange);

		if (isInRange)
		{
			std::cout << "Game Over, Hero is dead." << std::endl;
		}
		else
		{
			std::cout << "Hero is still alive." << std::endl;
		}
	}
};

#endif // Enemy_h