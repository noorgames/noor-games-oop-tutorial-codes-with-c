#ifndef Hero_h
#define Hero_h

#include "Position.cpp"

class Hero
{
private:
	Position		m_position;

public:
	Hero()
		: m_position(0, 0)
	{

	}

	Position getPosition() const
	{
		return m_position;
	}
};

#endif // Hero_h