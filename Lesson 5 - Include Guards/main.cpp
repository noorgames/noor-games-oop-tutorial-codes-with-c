#include "Position.h"
#include "Hero.h"
#include "Enemy.h"

int main()
{
	Hero spiderman;
	Enemy greenGoblin;

	std::cout << "Game is starting..." << std::endl;

	greenGoblin.attackHero(spiderman);

	return 0;
}