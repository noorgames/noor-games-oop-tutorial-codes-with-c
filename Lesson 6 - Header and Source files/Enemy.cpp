#include "Enemy.h"

Enemy::Enemy()
	: m_position(10, 0)
	, m_attackRange(5)
{

}

void Enemy::attackHero(const Hero& hero) const
{
	std::cout << "Enemy attacks..." << std::endl;

	const Position heroPosition = hero.getPosition();
	const bool isInRange = m_position.isInRange(heroPosition, m_attackRange);

	if (isInRange)
	{
		std::cout << "Game Over, Hero is dead." << std::endl;
	}
	else
	{
		std::cout << "Hero is still alive." << std::endl;
	}
}
