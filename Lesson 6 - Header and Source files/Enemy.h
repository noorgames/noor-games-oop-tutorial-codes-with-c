#ifndef Enemy_h
#define Enemy_h

#include <iostream>
#include "Position.h"
#include "Hero.h"

class Enemy
{
private:
	Position		m_position;
	float			m_attackRange;

public:
	Enemy();
	void attackHero(const Hero& hero) const;
};

#endif // Enemy_h