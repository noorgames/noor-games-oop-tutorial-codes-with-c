#ifndef Hero_h
#define Hero_h

#include "Position.h"

class Hero
{
private:
	Position		m_position;

public:
	Hero();
	Position getPosition() const;
};

#endif // Hero_h