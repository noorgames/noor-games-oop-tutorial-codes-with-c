#include "Position.h"

Position::Position(float x, float y)
	: m_x(x)
	, m_y(y)
{

}

bool Position::isInRange(const Position& other, const float range) const
{
	const float deltaX = other.m_x - m_x;
	const float deltaY = other.m_y - m_y;
	const float distanceSquare = deltaX * deltaX + deltaY * deltaY;
	const bool isInRange = distanceSquare <= range * range;

	return isInRange;
}


