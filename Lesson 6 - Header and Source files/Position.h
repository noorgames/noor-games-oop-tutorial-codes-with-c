#ifndef Position_h
#define Position_h

class Position
{
private:
	float	m_x;
	float	m_y;

public:
	Position(float x, float y);
	bool isInRange(const Position& other, const float range) const;
};

#endif // Position

