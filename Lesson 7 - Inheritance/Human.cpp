#include "Human.h"
#include <iostream>

Human::Human()
	: m_name("Armen")
	, m_age(25)
{

}

void Human::printName() const
{
	std::cout << "My name is " << m_name << std::endl;
}

void Human::printAge() const
{
	std::cout << "I am " << m_age << std::endl;
}

