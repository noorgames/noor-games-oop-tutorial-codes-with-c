#ifndef Student_h__
#define Student_h__
#include "Human.h"

class Student : public Human
{
public:
	Student();
	void printName() const;
	void study() const;
};


#endif // Student_h__



