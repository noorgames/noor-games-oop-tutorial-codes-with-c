#ifndef Human_h__
#define Human_h__

#include <string>

class Human
{
protected:
	std::string     m_name;
	int             m_age;

public:
	Human();
	virtual void printName() const;
	void printAge() const;
};



#endif // Human_h__

