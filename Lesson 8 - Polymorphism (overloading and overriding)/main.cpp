#include "Human.h"
#include "Student.h"
#include <iostream>

void print(const int i)
{
    std::cout << "integer: " << i << std::endl;
}

void print(const double d)
{
    std::cout << "double: " << d << std::endl;  
}


int main()
{
//     const int i = 5;
//     const double d = 1.2;
// 
//     print(i);
//     print(d);

    Human human;
    Student student;

    Human* people[] = { &human, &student };

    for (int i = 0; i < 2; ++i)
    {  
        people[i]->printName();
    }

    return 0;
}
