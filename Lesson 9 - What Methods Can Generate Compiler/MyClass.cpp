#include <iostream>


class MyClass
{
public:
    int*     m_i;

    MyClass()
    {

    }

    MyClass(const MyClass& other)
        :m_i(other.m_i)
    {

    }

    MyClass& operator=(const MyClass& other)
    {
        m_i = other.m_i;
        return *this;
    }

    ~MyClass()
    {

    }
};


int main()
{

    MyClass object1;
    object1.m_i = new int[100];

    MyClass object2;
    object2 = object1;

    //     MyClass defaultObject;
    //     defaultObject.m_i = 50;
    // 
    //     MyClass copyObject(defaultObject);
    //     std::cout << "Copy: " << copyObject.m_i << std::endl;
    // 
    //     MyClass assignObject;
    //     defaultObject = assignObject = copyObject;
    //     std::cout << "Assign: " << assignObject.m_i << std::endl;
    // 
    //     MyClass otherObject = defaultObject;

    return 0;
}
